﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SueloController : MonoBehaviour
{
    // Start is called before the first frame update

    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            Debug.LogWarning("No tiene AudioSOurce");
        }
    }

    // Update is called once per frame
    /*
    void Update()
    {
        
    }
    */

    static float Speed2pitch(float speed)
    {
        float normalised = Mathf.InverseLerp(2.0f, 8.0f, speed);
        return Mathf.Lerp(0.333f, 1.666f, normalised);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bomb"))
        {
            BombController bc;
            bc = other.GetComponent<BombController>();
            if (bc != null)
            {
                audioSource.pitch = Speed2pitch(bc.speed);
            }
            else
            {
                audioSource.pitch = 1.0f;
            }
            if (audioSource != null)
            {
                audioSource.Play();
            }
        }
    }
}
