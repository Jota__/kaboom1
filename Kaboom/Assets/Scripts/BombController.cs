﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour
{
    [ Tooltip("Velocidad de caída de la bomba")]
    public float speed = 3f;
    [Tooltip("Máxima Y permitida")]
    //public float yLimit = -1.5f;

    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (rb == null)
        {
            enabled = false;
        }
    }

    
    void FixedUpdate()
    {
        Vector3 currentPos = rb.position;
        currentPos.y -= speed * Time.deltaTime;
        rb.MovePosition(currentPos);

        /*
        Transform t = GetComponent<Transform>();
        Vector3 pos = t.position;
        //pos.y = pos.y - speed * Time.deltaTime;
        t.position = pos;
        /*
        GameObject player;
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            Collider collider = player.GetComponent<Collider>();
            if (collider != null)
            {
                if (collider.bounds.Contains(transform.position))
                {
                    Catched();
                    return;
                }
            }
        }
        */
        /*
        if (Grounded())
        {
           
            foreach (GameObject go in GameObject.FindGameObjectsWithTag("Bomb"))
            {
                BombController bc;
                bc = go.GetComponent<BombController>();
                if (bc != null)
                {
                    bc.Explode();
                }
            }
           
        }
       */
    }

    /*
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Catched();
        }
    }
    */
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Catched();
        }
        else
        {
            GameManager.Instance.BombGrounded();
        }
    }

    /*
    bool Grounded()
    {

        return transform.position.y <= yLimit;

    }
    */
    public void Explode()
    {
        Destroy(this.gameObject);
    }
    
    void Catched()
    {
        GameManager.Instance.BombCatched();
        Destroy(this.gameObject);
    }


}
