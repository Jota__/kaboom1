﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update

    private AudioSource audioSource;

    public GameObject topBar;
    public GameObject middleBar;
    public GameObject bottomBar;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            Debug.LogWarning("No tiene AudioSOurce");
        }

        //setNumBars(3);
    }

    public void setNumBars(uint num)
    {
        if (num > 3)
        {
            Debug.LogError("El número de vidas no puede ser más de tres");
            return;
        }

        topBar.SetActive(num > 0);
        middleBar.SetActive(num > 1);
        bottomBar.SetActive(num > 2);
        /*
        BoxCollider c = GetComponent<BoxCollider>();
        c.center = new Vector3(0.0f, -0.25f + (num - 1), 0.0f);
        float height;
        if (num != 0)
        {
            height = 0.5f * num - 0.25f;
        }
        else
        {
            height = 0.0f;
        }
        c.size = new Vector3(2.0f, height, 1.0f);
        */
    }

    // Update is called once per frame
    /*
    void Update()
    {
        
    }
    */

    static float Speed2pitch(float speed)
    {
        float normalised = Mathf.InverseLerp(2.0f, 8.0f, speed);
        return Mathf.Lerp(0.333f, 1.666f, normalised);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bomb"))
        {
            BombController bc;
            bc = other.GetComponent<BombController>();
            if (bc != null)
            {
                audioSource.pitch = Speed2pitch(bc.speed);
            }
            else
            {
                audioSource.pitch = 1.0f;
            }
            if (audioSource != null)
            {
                audioSource.Play();
            }
        }
    }
}
